#!/usr/bin/env python3
# -*- coding: utf8 -*-

import os.path
import sys

# Resources:
# https://wiki.lineageos.org/proprietary_blobs.html
# [-]source[:destination][|sha1sum]

helpmessage="""
Check if all files specified in proprietary-files.txt are actually
existing.

Example call:
python3 check_proprietary.py < path_to_proprietary-files.txt> <path_to_proprietary files>
python3 check_proprietary.py <path_to_lineage>/device/fairphone/fp3/proprietary-files.txt <path_to_lineage>/vendor/fairphone/fp3/proprietary/

"""

def check_proprietary_files( spec, folder):

    files_found = 0
    files_missing = 0

    with open(spec, 'r') as myfile:
        lines = myfile.readlines()
        success = True
        for line in lines:
            result = check_line(line, folder)

            if result == True:
                #print(f'Success : {line.strip()}')
                files_found += 1
            elif result == None:
                #print(f'Ignore  : {line.strip()}')
                pass
            elif result == False:
                print(f'Fail    : {line.strip()}')
                files_missing += 1
                success = False
            else:
                print(f'Error: Found something else {result}') # might be a leftover mock
                raise SystemError('Something weird is going on')

    if success:
        print(f'Success: All {files_found} specified files are existing')
    else:
        print(f'\nError: Some files are missing ( {files_missing} missing, ' \
            + f'{files_found} found ) please extract again')

    return success


def check_line ( line, path ):
    """Return True or False wether file was found;
    Return None for empty and comments"""

    line = line.strip()
    filename = None

    if line.startswith('-'):
        line = line.lstrip('-')

    if not line:
        return None
    elif line[0] == '#'or line.isspace():
        return None
    elif ':' in line and '|' in line:
        nameandsha = line.split(':')[1]
        filename = nameandsha.split('|')[0]
    elif ':' in line:
        filename = line.split(':')[1] #check the destination file
    elif '|' in line:
        filename = line.split('|')[0]
    else:
        filename = line

    thefile = os.path.join(path, filename)

    if os.path.exists(thefile):
        return True
    else:
        print(f'Not found {thefile}')
        return False


if __name__=='__main__':
    if len(sys.argv) == 3:
        if check_proprietary_files(sys.argv[1], sys.argv[2]):
            sys.exit(0)
        else:
            sys.exit(1)
    else:
        print(helpmessage)

