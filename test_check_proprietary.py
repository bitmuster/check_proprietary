import unittest
import os, os.path
from unittest.mock import ANY, patch, call, MagicMock
from check_proprietary import check_proprietary_files, check_line

class TestProprietary(unittest.TestCase):

    def setUp(self):
        self.patchcheckline = patch('check_proprietary.check_line')
        self.patchopen = patch('check_proprietary.open', name='mockopen')
        self.mockcheckline = self.patchcheckline.start()
        self.mockopen = self.patchopen.start()

    def tearDown(self):
        self.mockcheckline.stop()
        self.mockopen.stop()

    def test_check_proprietaryfiles(self):
        self.assertEqual(check_proprietary_files(ANY, ANY), True)
        self.mockopen.assert_called_with(ANY, ANY)

    def test_check_proprietary_files_read(self):
        mockfile = MagicMock(name='mockfile')
        mockreadlines = MagicMock(name='mockreadlines', return_value=['fooread'])
        mockfile.readlines = mockreadlines
        self.mockcheckline.return_value = True

        # open returns a context manager and one that returns the mocked file
        self.mockopen.return_value.__enter__.return_value = mockfile

        self.assertEqual(check_proprietary_files(ANY, ANY), True)

        mockreadlines.assert_called()
        self.mockcheckline.assert_called_with('fooread', ANY)

    def test_check_proprietary_files_read_lines(self):
        mockfile = MagicMock(name='mockfile')
        mockreadlines = MagicMock(name='mockread', return_value = ['line1','line2'])
        mockfile.readlines = mockreadlines
        self.mockcheckline.return_value = True

        self.mockopen.return_value.__enter__.return_value = mockfile

        self.assertEqual(check_proprietary_files(ANY, ANY), True)

        mockreadlines.assert_called()

        # we observe also that: call().__bool__() -> ignore
        # mockcheckline.assert_has_calls(
        #    [call('line1', ANY), call('line2', ANY)]
        #    )
        self.mockcheckline.assert_any_call( 'line1', ANY )
        self.mockcheckline.assert_any_call( 'line2', ANY )

    def test_check_proprietary_files_read_lines_fail(self):
        mockfile = MagicMock(name='mockfile')
        mockreadlines = MagicMock(name='mockread', return_value = ['line1','line2'])
        mockfile.readlines = mockreadlines
        self.mockcheckline.side_effect = [True, False]

        self.mockopen.return_value.__enter__.return_value = mockfile

        self.assertEqual(check_proprietary_files(ANY, ANY), False)

        mockreadlines.assert_called()
        self.mockcheckline.assert_has_calls(
            [call('line1', ANY), call('line2', ANY)]
            )

    def test_check_proprietary_files_read_lines_none(self):
        mockfile = MagicMock(name='mockfile')
        mockreadlines = MagicMock(name='mockread', return_value = ['line1','line2'])
        mockfile.readlines = mockreadlines
        self.mockcheckline.side_effect = [None, True]

        self.mockopen.return_value.__enter__.return_value = mockfile

        self.assertEqual(check_proprietary_files(ANY, ANY), True)

        mockreadlines.assert_called()
        self.mockcheckline.assert_has_calls(
            [call('line1', ANY), call('line2', ANY)]
            )

    def test_check_proprietary_files_read_nonsense(self):
        mockfile = MagicMock(name='mockfile')
        mockreadlines = MagicMock(name='mockread', return_value = [ 123 ])
        mockfile.readlines = mockreadlines

        self.mockopen.return_value.__enter__.return_value = mockfile

        with self.assertRaises( SystemError ):
            check_proprietary_files(ANY, ANY)


class TestCheckLine(unittest.TestCase):

    def test_check_entry_comment(self):
        self.assertEqual(check_line('#Comment', None), None)

    def test_check_entry_empty(self):
        self.assertEqual(check_line('', None), None)

    def test_check_entry_empty_spaces(self):
        self.assertEqual(check_line('    ', None), None)

    def test_check_entry_empty_tabs(self):
        self.assertEqual(check_line('\t', None), None)

    @patch('os.path.exists')
    def test_check_entry_file(self, mock):
        myfile = 'vendor/bin/somefile'
        path = '/abspath'
        self.assertEqual(check_line(myfile, path), True)
        mock.assert_called_with(os.path.join(path, myfile))

    @patch('os.path.exists')
    def test_check_entry_file_with_whitespace(self, mock):
        myfile = 'vendor/bin/somefile    '
        path = '/abspath'
        self.assertEqual(check_line(myfile, path), True)
        mock.assert_called_with(os.path.join(path, myfile.strip()))

    @patch('os.path.exists')
    def test_check_entry_file_missing(self, mock):
        mock.return_value = False
        myfile = 'vendor/bin/somefile'
        path = '/abspath'
        self.assertEqual(check_line(myfile, path), False)
        mock.assert_called_with(os.path.join(path, myfile))

    @patch('os.path.exists')
    def test_check_entry_nonsense(self, mock):
        mock.return_value = False
        myfile = 'abc'
        path = 'xyz'
        self.assertEqual(check_line(myfile, path), False)
        mock.assert_called_with(os.path.join(path, myfile))

    @patch('os.path.exists')
    def test_check_entry_sha1sum(self, mock):
        path = '/abspath'
        line = 'foldera/folderb/filename.xml|2826b33ed8e2756cc748c80bd6c466ff8af97ac5'
        self.assertEqual(check_line(line, path), True)
        mock.assert_called_with(os.path.join(path, 'foldera/folderb/filename.xml'))

    @patch('os.path.exists')
    def test_check_entry_sha1sum_missing(self, mock):
        mock.return_value = False
        path = '/abspath'
        line = 'foldera/folderb/filename.xml|2826b33ed8e2756cc748c80bd6c466ff8af97ac5'
        self.assertEqual(check_line(line, path), False)
        mock.assert_called_with(os.path.join(path, 'foldera/folderb/filename.xml'))

    @patch('os.path.exists')
    def test_check_entry_minus(self, mock):
        line = '-vendor/bin/somefile'
        path = '/abspath'
        tocheck = os.path.join(path, line[1:])
        self.assertEqual(check_line(line, path), True)
        mock.assert_called_with(tocheck)

    @patch('os.path.exists')
    def test_check_entry_sha1sum_2(self, mock):
        filename = 'vendor/bin/somefile'
        line = filename + '|' + 'a'*40
        path = '/abspath'
        tocheck = os.path.join(path, filename)
        self.assertEqual(check_line(line, path), True)
        mock.assert_called_with(tocheck)

    @patch('os.path.exists')
    def test_check_entry_sha1sum_and_minus(self, mock):
        filename = 'vendor/bin/somefile'
        line = '-' + filename + '|' + 'a'*40
        path = '/abspath'
        tocheck = os.path.join(path, filename)
        self.assertEqual(check_line(line, path), True)
        mock.assert_called_with(tocheck)

    @patch('os.path.exists')
    def test_check_entry_colon(self, mock):
        filename = 'vendor/bin/sourcefile'
        filenamedest = 'vendor/bin/destfile' # we expect this one to be there
        line = filename + ':' + filenamedest
        path = '/abspath'
        tocheck = os.path.join(path, filenamedest)
        self.assertEqual(check_line(line, path), True)
        mock.assert_called_with(tocheck)

    @patch('os.path.exists')
    def test_check_entry_colon_and_shasum(self, mock):
        filename = 'vendor/bin/sourcefile'
        filenamedest = 'vendor/bin/destfile'
        line = filename + ':' + filenamedest + '|' + 'a'*40
        path = '/abspath'
        tocheck = os.path.join(path, filenamedest)
        self.assertEqual(check_line(line, path), True)
        mock.assert_called_with(tocheck)
