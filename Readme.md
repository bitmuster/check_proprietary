
# Check Proprietary Files for LineageOS Devices

This project shall support the development process of LineaeOS devices. 
The intention is to cross check if all proprietary files
specified in 'proprietary-files.txt' are also existent in the required folder.


Call example:

    python3 check_proprietary.py < path_to-proprietary_files.txt> <path_to_proprietary_files>
    python3 check_proprietary.py path_to_build_dir/device/phone/type/proprietary-files.txt path_to_build_dir/vendor/phone/type/proprietary/

Run tests:

    python3 -m unittest test_check_proprietary.py

